package org.vertexprize.education.spring.graphql.server;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.vertexprize.education.spring.graphql.server.service.DatabaseService;

@SpringBootApplication
public class GraphqlServerApplication {

    private static final Logger log = LoggerFactory.getLogger(GraphqlServerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(GraphqlServerApplication.class, args);
    }

    @Autowired
    private DatabaseService databaseService;

    @PostConstruct
    public void start() {
        log.info("Формирование базы данных студентов в памяти ...");
        databaseService.init();

    }

}
