/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.graphql.server.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import org.vertexprize.education.spring.graphql.server.entity.Student;
import org.vertexprize.education.spring.graphql.server.service.DatabaseService;

/**
 *
 * @author vaganovdv
 */
@Controller
public class StudentController {
    
    private final DatabaseService databaseService;

    public StudentController(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }
    
    
    /**
     * Контроллер для получения полного списка студентов 
     * Имя метода [students] и сигнатура должны совпадать с сигнатурой в файле 
     * 
     * 
     * @return 
     */
    @QueryMapping
    public List<Student> students() {
        return databaseService.getAllStundents();
    }
        
    /**
    * Контроллер для получения единичного экземпляра Student по идентификатору
    */
    @QueryMapping
    public Student getStudentById(@Argument String id) {
        Student found = null;
        Optional<Student> foundOpt = databaseService.getById(id);
        if (foundOpt.isPresent()) {
            found = foundOpt.get();
            
        }
        return found;
    }
    
    
    //getStudentById
    
    
    
}
