/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.graphql.server.entity;

import lombok.Data;
import lombok.ToString;



/**
 *  Класс Student при использовании JPA
 * 
 * @author vaganovdv
 */


@Data
@ToString
public class Student {
    
    private String id; // Идентификатор записи студента 
    
    // Поля класса Student
    //
    private String firstName;
    private String sureName;
    private String middleName;
    private String studentGroup;
    private int  year;
    
}
