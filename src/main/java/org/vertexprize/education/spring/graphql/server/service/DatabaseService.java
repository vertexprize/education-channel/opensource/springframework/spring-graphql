/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.graphql.server.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.vertexprize.education.spring.graphql.server.entity.Student;

/**
 *
 * @author vaganovdv
 */
@Service
public class DatabaseService {

    private static final Logger log = LoggerFactory.getLogger(DatabaseService.class);

    /**
     * Организация базы студентов в виде массива
     */
    private final List<Student> students = new ArrayList<>();

    public void init() {
        log.info("Добавление студентов ... ");

        Student student1 = new Student();
        student1.setId(UUID.randomUUID().toString());
        student1.setFirstName("Дмитрий");
        student1.setMiddleName("Романович");
        student1.setSureName("Татарчук");
        student1.setStudentGroup("ПГС-21");
        student1.setYear(5);

        Student student2 = new Student();
        student2.setId(UUID.randomUUID().toString());
        student2.setFirstName("Иван");
        student2.setMiddleName("Иванович");
        student2.setSureName("Петров");
        student2.setStudentGroup("ПГС-21");
        student2.setYear(5);

        Student student3 = new Student();
        student3.setId(UUID.randomUUID().toString());
        student3.setFirstName("Кирилл");
        student3.setMiddleName("Романович");
        student3.setSureName("Кичигин");
        student3.setStudentGroup("ПГС-21");
        student3.setYear(5);

        students.add(student1);
        students.add(student2);
        students.add(student3);
    }

    /**
     * Получение полного списка студентов
     *
     * @return
     */
    public List<Student> getAllStundents() {
        return students;
    }

    public Optional<Student> getById(String id) {
        log.info("Поиск студента по идентификатору id = " + id);
        Optional<Student> foundStudent = students.stream().filter(s -> s.getId().equalsIgnoreCase(id)).findFirst();
        if (foundStudent.isEmpty()) {
            log.error("Не найден студент с идентификатором id = " + id);
        }
        return foundStudent;
    }

}
